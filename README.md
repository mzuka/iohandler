### Summary ###
The goal of this project is to provide a way through which python devs can  write IO tasks 
seamlessly without worrying much about python version compatibility issues'
### Version ###
1.0.1

### How do I get set up? ###
run pip install iohandler
### How to run tests ###
[Nose](https://nose.readthedocs.org/en/latest/) is already integrated with setup.py. To run 
tests, we can simply do:
~~~
$ python setup.py test
~~~

### Contribution guidelines ###

* Writing tests
TBA
* Code review
TBA
* Other guidelines
TBA

### Who do I talk to? ###

* admin: mzukafull@gmail.com
* Other :
mzukafull@gmail.com