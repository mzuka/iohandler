# coding=utf-8
import unittest

from collections import Iterable

from iohandler.IOFile import IOFileHandler
from io import IOBase
import os

logs_path = os.path.dirname(__file__) + "{sep}logs{sep}" \
    .format(
    sep=os.sep
)
pkg_path = os.path.dirname(__file__) + "{sep}pkg{sep}" \
    .format(
    sep=os.sep
)
pkgs_path = os.path.dirname(__file__) + "{sep}pkgs{sep}" \
    .format(
    sep=os.sep
)


class MyTestCase(unittest.TestCase):
    def test_io_object(self):
        fph = IOFileHandler()
        x = fph.open(file_name='test.log', file_path=logs_path, create_file_if_none=True,
                     create_package=False, mode='ab+')
        self.assertIsInstance(x, IOBase)
        x.close()

    def test_io_object_create_and_write(self):
        fph = IOFileHandler()
        x = fph.open(file_name='testtyu.log', file_path=logs_path, create_file_if_none=True,
                     create_package=False, mode='ab+')
        eng = "The quick brown fox jumped over the lazy dog "
        y = fph.write(x, eng)
        self.assertIsInstance(y, bool)
        x.close()

    def test_io_object_create_and_write_package(self):
        fph = IOFileHandler()
        x = fph.open(file_name='pack.py', file_path=pkgs_path, create_file_if_none=True,
                     create_package=True, mode='ab+')
        eng = "import os "
        y = fph.write(x, eng)
        self.assertIsInstance(y, bool)
        x.close()

    # def test_io_object_create_and_write_package_noaccess(self):
    #     fph = IOFileHandler()
    #     x = fph.open(file_name='pack.py', file_path=pkg_path, create_file_if_none=True,
    #                  create_package=True, mode='ab+')
    #     eng = "import os "
    #     with self.assertRaises(SystemExit) as se:
    #         result = fph.write(x, eng)
    #     self.assertEqual(se.exception.args[-1],"SystemExit: IOFileHandler:new_file | ERR9_902. "
    #     "Create Package Error.\nMessage: Permission denied")
    #     x.close()

    def test_io_object_no_create(self):
        fph = IOFileHandler()
        with self.assertRaises(SystemExit) as se:
            fph.open(file_name='testss.log',
                     file_path=logs_path,
                     create_file_if_none=False,
                     create_package=False,
                     mode='ab+')
        self.assertEqual(se.exception.code,
                         '{0}:open | ERR901. File Not Found. Not Instructed to create new.'.format(
                             type(fph).__name__))

    def test_io_object_write_non_ascii(self):
        fph = IOFileHandler()
        urdu = "میں اگلے چند ماہ میں آپ کے ساتھ کام کرنے کے لئے حوصلہ افزائی پچھلے ہفتے بہت "
        urdu_file = fph.open(file_name='urdu.log', file_path=logs_path, create_file_if_none=True,
                             create_package=False, mode='ab+')
        result = fph.write(urdu_file, urdu)
        urdu_file.close()
        self.assertIsInstance(result, bool)

    def test_io_object_write_ascii(self):
        fph = IOFileHandler()
        eng = "The quick brown fox jumped over the lazy dog "
        eng_file = fph.open(file_name='eng.log', file_path=logs_path, create_file_if_none=True,
                            create_package=False, mode='ab+')
        result = fph.write(eng_file, eng)
        eng_file.close()
        self.assertIsInstance(result, bool)

    def test_io_object_write_closed(self):
        fph = IOFileHandler()
        eng = "The quick brown fox jumped over the lazy dog "
        eng_file = fph.open(file_name='eng.log', file_path=logs_path, create_file_if_none=True,
                            create_package=False, mode='ab+')
        eng_file.close()
        with self.assertRaises(SystemExit) as se:
            fph.write(eng_file, eng)
        self.assertEqual(se.exception.args[-1], "E9702|Object lacks support for Write Operation(s)")

    def test_io_object_read_json_no_buffer(self):
        fph = IOFileHandler()
        test_data_object = fph.open('test.json', file_path=os.path.dirname(__file__),
                                    create_file_if_none=False,
                                    mode='r+')
        y = fph.read(test_data_object)
        self.assertIsInstance(y, Iterable)

    def test_io_object_readlines_json_no_buffer(self):
        fph = IOFileHandler()
        test_data_object = fph.open('test.json', file_path=os.path.dirname(__file__),
                                    create_file_if_none=False,
                                    mode='r+')
        y = fph.readlines(test_data_object)
        self.assertIsInstance(y, Iterable)

    def test_io_object_read_json_with_buffer(self):
        fph = IOFileHandler()
        test_data_object = fph.open('test.json', file_path=os.path.dirname(__file__),
                                    create_file_if_none=False,
                                    mode='rb+')
        y = fph.read(test_data_object)
        self.assertIsInstance(y, Iterable)

    def test_io_object_invalid_io(self):
        fph = IOFileHandler()
        test_data_object = 235
        with self.assertRaises(SystemExit) as se:
            fph.read(test_data_object)
        self.assertEqual(se.exception.code, "ERR904:Invalid Stream.")

    def test_io_object_invalid_io_readlines(self):
        fph = IOFileHandler()
        test_data_object = 235
        with self.assertRaises(AttributeError) as se:
            fph.readlines(test_data_object)
        self.assertEqual(se.exception.args[-1], "ERR904:Invalid Stream.")
