from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(name='iohandler',
      version='1.0.5',
      description='The goal of this project is to provide a way through which python devs can '
                  'write IO tasks seamlessly without worrying much about 2/3 compatibility issues',
      long_description=readme(),
      classifiers=[
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3',
          'Topic :: Software Development :: Libraries :: Python Modules',
      ],
      keywords='io str unicode bytes iohandler BaseIO',
      url='https://bitbucket.org/mzuka/iohandler',
      author='Mzuka',
      author_email='mzukafull@gmail.com',
      license='MIT',
      packages=['iohandler'],
      install_requires=[
          'typing',
          'pathlib'
      ],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      entry_points={
          # 'console_scripts': ['funniest-joke=funniest.command_line:main'],
      },
      include_package_data=True,
      zip_safe=False)
